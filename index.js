const express = require('express')
const cors = require('cors')
const app = express()
const { default: mongoose } = require('mongoose')
require('dotenv').config()
const routes = require('./src/routes/index')
const routesApi = require('./src/routes/api')
const swaggerUi = require('swagger-ui-express')
const swaggerJsdoc = require('swagger-jsdoc')

app.use(express.json())
routes.setup(app)
routesApi.setup(app)

// CORS
app.use(
  cors({
    origin: '*',
  }),
)
// add to variable env
app.set('secretKey', process.env.SECRETKEY) // jwt secret token
// CONFIG DATA BASE
mongoose.connect(process.env.MONGODB_URI)
mongoose.connection.on('error', (err) => {
  console.error(err)
  console.log(
    '%s MongoDB connection error. Please make sure MongoDB is running.',
  )
  process.exit()
})

//SWAGGER DOCS

// we separate each swagger url for security reasons
const options = {
  failOnErrors: true, // Whether or not to throw when parsing errors. Defaults to false.
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'API pointage ADMIN',
      version: '1.0.0',
    },
    servers: [
      {
        //TODO add variabel env
        url: 'http://localhost:4005',
        description: 'Development server',
      },
    ],
  },
  apis: ['./src/routes/index.js'], // files containing annotations as above
}
const optionsApi = {
  failOnErrors: true, // Whether or not to throw when parsing errors. Defaults to false.

  definition: {
    openapi: '3.0.0',
    info: {
      title: 'API pointage apis',
      version: '1.0.0',
    },

    servers: [
      {
        url: 'http://localhost:4005',
        description: 'Development server',
      },
    ],
    components: {
      securitySchemes: {
        'x-access-token': {
          type: 'apiKey',
          name: 'x-access-token',
          scheme: 'bearer',
          in: 'header',
        },
      },
    },
  },
  apis: ['./src/routes/api.js'], // files containing annotations as above
}
const specs = swaggerJsdoc(options)
const specsApi = swaggerJsdoc(optionsApi)

app.use(
  process.env.SWAGGER_PATH,
  swaggerUi.serve,
  (...args) => swaggerUi.setup(specs)(...args),
)
app.use(
  process.env.SWAGGER_PATH_API,
  swaggerUi.serve,
  (...args) => swaggerUi.setup(specsApi)(...args),
)

// JSON parse

// file env
const port = process.env.PORT
app.listen(port, () => {
  console.log(
    'We are started on : http://localhost:' + port,
  )
  console.log(
    'swagger are started on : http://localhost:' +
      port +
      process.env.SWAGGER_PATH,
  )
  console.log(
    'swagger api are started on : http://localhost:' +
      port +
      process.env.SWAGGER_PATH_API,
  )
})
