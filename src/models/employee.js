const mongoose = require("mongoose");
//Define a schema
const Schema = mongoose.Schema;
const employeeSchema = new Schema(
  {
    id: {
      type: String,
      trim: true,
      required: true
    },
    firstName: {
      type: String,
      trim: true,
      required: true
    },
    name: {
      type: String,
      trim: true,
      required: true
    },
    dateCreated: {
      type: Date,
      trim: true,
      required: true
    },
    department: {
      type: String,
      trim: true,
      required: true
      
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Employee", employeeSchema);
