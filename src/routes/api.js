const employeeModel = require('../controllers/employee')
const jwt = require('jsonwebtoken')
module.exports.setup = function (app, db) {
  /**
   * @swagger
   * /checkapi:
   *   get:
   *     description: check api connexion
   *     tags: [checkapi]
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       204:
   *         description: No Content
   */
  app.get('/checkapi', (req, res) => {
    res.send('Hello World!')
    return 'healthy'
  })
  //********************** TO REMOVE COMMENT ******************** */
  app.use('/api/*', function (req, res, next) {
    try {
      var decoded = jwt.verify(
        req.headers['x-access-token'],
        req.app.get('secretKey'),
      )
      req.body.customer_id = decoded.id
      next()
    } catch (err) {
      console.log(err)
      return res.status(401).json({
        status: 'error',
        data: 'token_error',
      })
    }
  })
  //****************************************** */

  /**
   * @swagger
   * /api/employee:
   *   post:
   *     tags: [employees]
   *     summary: Create a empolyee.
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               id:
   *                 type: string
   *                 description: id d' empolyee.
   *                 example: 001
   *               department:
   *                 type: string
   *                 description: department d' empolyee.
   *                 example: department
   *               dateCreated:
   *                 type: date
   *                 description: date de cration .
   *                 example: Thu Sep 22 2022 22:50:29 GMT+0100 (West Africa Standard Time)
   *               firstName:
   *                 type: string
   *                 description: firstName empolyee.
   *                 example: tarek
   *               name:
   *                 type: string
   *                 description: name empolyee .
   *                 example: mdimagh
   *
   *     responses:
   *       200:
   *         description: Success
   *       204:
   *         description: No Content
   */

  app.post(
    '/api/employee',
    // employeeModel.validate("insertEmployee"),
    employeeModel.create,
  )

  /**
   * @swagger
   * /api/employee/{id}:
   *   put:
   *     tags: [employees]
   *     summary: Create a empolyee.
   *     parameters:
   *       - in: path
   *         name: id
   *         required: true
   *         description: Numeric ID of the user to retrieve.
   *         schema:
   *           type: integer
   *     requestBody:
   *       required: true
   *       content:
   *
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               department:
   *                 type: string
   *                 description: department d' empolyee.
   *                 example: department
   *               dateCreated:
   *                 type: date
   *                 description: date de cration .
   *                 example: Thu Sep 22 2022 22:50:29 GMT+0100 (West Africa Standard Time)
   *               firstName:
   *                 type: string
   *                 description: firstName empolyee.
   *                 example: tarek
   *               name:
   *                 type: string
   *                 description: name empolyee .
   *                 example: mdimagh
   *
   *     responses:
   *       200:
   *         description: Success
   *       204:
   *         description: No Content
   */

  app.put('/api/employee/:id', employeeModel.update)

  /**
   * @swagger
   * /api/employees:
   *   get:
   *     tags: [employees]
   *     summary: Create a empolyee.
   *     parameters:
   *       - in: query
   *         name: department
   *         description: department name.
   *         schema:
   *           type: string
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *         description: No Content
   */
  app.get('/api/employees', employeeModel.getList)
}
