const adminController = require("../controllers/admin");
module.exports.setup  = function(app, db) {
    /**
     * @swagger
     * /check:
     *   get:
     *     description: check api connexion
     *     tags: [check]
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Success
     *       204:
     *         description: No Content
    */
    app.get('/check', (req, res) => { 
        res.send('Hello World!');
        return "healthy"
      });

/**
    * @swagger
    * /authenticate:
    *   post:
    *     summary: Create a JSONPlaceholder admin.
    *     requestBody:
    *       required: true
    *       content:
    *         application/json:
    *           schema:
    *             type: object
    *             properties:
    *               email:
    *                 type: string
    *                 description: email admin.
    *                 example: admin@admin.com
    *               password:
    *                 type: string
    *                 description: password .
    *                 example: 1234567
    *     responses:
    *       200:
    *         description: Success
    *       204:
    *         description: No Content
    */
   app.post(
    "/authenticate",
    adminController.validate("loginAdmin"),
    adminController.authenticate
  );


  /**
    * @swagger
    * /createAdmin:
    *   post:
    *     summary: Create a JSONPlaceholder admin.
    *     requestBody:
    *       required: true
    *       content:
    *         application/json:
    *           schema:
    *             type: object
    *             properties:
    *               email:
    *                 type: string
    *                 description: email admin.
    *                 example: admin@admin.com
    *               password:
    *                 type: string
    *                 description: password .
    *                 example: 1234567
    *               firstName:
    *                 type: string
    *                 description: firstName admin.
    *                 example: tarek
    *               lastName:
    *                 type: string
    *                 description: firstName admin .
    *                 example: mdimagh
    * 
    *     responses:
    *       200:
    *         description: Success
    *       204:
    *         description: No Content
    */
   

          app.post(
            "/createAdmin", 
            adminController.validate("registerAdmin"),
            adminController.create
          );  
  };