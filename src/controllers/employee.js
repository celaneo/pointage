const employeeModel = require('../models/employee')
const {
  validationResult,
  check,
} = require('express-validator/check')

module.exports = {
  create: async function (req, res, next) {
    //check and validat request payload body
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res
        .status(422)
        .json({ errors: errors.array() })
    }
    //check employee is saved in database or not with ref:id
    const empoyee = await employeeModel.findOne({
      id: req.body.id,
    })
    // return in this case that employee already saved s
    if (empoyee) {
      res.json({
        status: 409,
        message: `employee with id : ${req.body.id}  already saved !`,
      })
      return
    }
    // or save the new employee
    employeeModel.create(
      {
        id: req.body.id,
        firstName: req.body.firstName,
        name: req.body.name,
        dateCreated: req.body.dateCreated,
        department: req.body.department,
      },
      function (err, result) {
        if (err) next(err)
        else {
          res.json({
            status: 'success',
            message: 'employee added successfully!!!',
          })
        }
      },
    )
  },

  update: async function (req, res, next) {
    //check and validat request payload body
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res
        .status(422)
        .json({ errors: errors.array() })
    }
    // find employee with id and update at the same time
    const empoyee = await employeeModel.findOneAndUpdate(
      { id: req.params.id },
      //we should update all employees info
      //Todo : check evry field and update that spesefic field
      req.body,
    )
    if (!empoyee) {
      res.json({
        status: 401,
        message: `employee not found !`,
      })
      return
    }
    res.json({
      status: 200,
      message: `employee was updated!`,
    })
  },

  getList: async function (req, res, next) {
    //check and validat request payload body
    const errors = validationResult(req.query)
    if (!errors.isEmpty()) {
      return res
        .status(422)
        .json({ errors: errors.array() })
    }
    // check if api have query params to filtred data with "department"
    let query = {}
    if (req.query.department) {
      query.department = req.query.department
    }
    const empoyees = await employeeModel.find(query)
    if (!empoyees) {
      res.json({
        status: 401,
        message: `No employees`,
        data: [],
      })
      return
    }
    res.json({
      status: 200,
      message: `employees list`,
      data: empoyees,
    })
  },
  validate: (method) => {
    switch (method) {
      case 'insertEmployee': {
        return [
          check('firstName').isLength({ min: 3 }),
          check('name').isLength({ min: 3 }),
          check('department').isLength({ max: 50 }),
          check('dateCreated').isDate(),
        ]
      }
    }
  },
}
