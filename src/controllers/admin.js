const adminModel = require('../models/admins.js')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const {
  validationResult,
  check,
} = require('express-validator/check')

module.exports = {
  create: function (req, res, next) {
    //check and validat request payload body
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res
        .status(422)
        .json({ errors: errors.array() })
    }
    adminModel.create(
      {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: req.body.password,
      },
      function (err, result) {
        if (err) next(err)
        else {
          res.json({
            status: 'success',
            message: 'admin added successfully!!!',
          })
        }
      },
    )
  },
  //to add this function like midleware to ckeck token acces
  verify: async function (req, res, next) {
    try {
      var jwtAdmin = jwt.verify(
        req.headers['x-access-token'],
        req.app.get('secretKey'),
      )
      req.jwt = jwtAdmin
      next()
    } catch (error) {
      return res.json({
        status: 'error',
        error,
      })
    }
  },
  //to get and generate token with avilbility 24h to use Api
  authenticate: function (req, res, next) {
    adminModel.findOne(
      { email: req.body.email },
      function (err, adminInfo) {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          return res
            .status(422)
            .json({ errors: errors.array() })
        }

        if (err) {
          next(err)
        } else {
          if (adminInfo !== null) {
            if (
              req.body !== null &&
              req.body.password !== null
            ) {
              if (
                bcrypt.compareSync(
                  req.body.password,
                  adminInfo.password,
                )
              ) {
                const token = jwt.sign(
                  {
                    id: adminInfo._id,
                    firstname: adminInfo.firstname,
                    lastname: adminInfo.lastname,
                    email: adminInfo.email,
                  },
                  req.app.get('secretKey'),
                  { expiresIn: process.env.TOKENVALIDITY },
                )

                res.json({
                  status: 'success',
                  message: 'admin found!!!',
                  data: { admin: adminInfo, token: token },
                })
              } else {
                res.json({
                  status: 'error',
                  message: 'Invalid email/password!!!',
                })
              }
            } else {
              res.json({
                status: 'error',
                message: 'Data null!!!',
              })
            }
          } else {
            res.json({
              status: 'error',
              message: "email doesn't exist",
            })
          }
        }
      },
    )
  },
  //validation payload
  validate: (method) => {
    switch (method) {
      case 'loginAdmin': {
        return [
          check('email').isEmail(),
          // password must be at least 5 chars long
          check('password')
            .isLength({ min: 5 })
            .withMessage('must be at least 5 chars long'),
        ]
      }
      case 'registerAdmin': {
        return [
          check('firstname').isLength({ min: 3 }),
          check('lastname').isLength({ min: 3 }),
          check('email').isEmail(),
          // password must be at least 5 chars long
          check('password')
            .isLength({ min: 5 })
            .withMessage('must be at least 5 chars long'),
        ]
      }
    }
  },
}
