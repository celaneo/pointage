# Pointage APIs

we create a simple endpoint to allow admin to insert and get list of employee and
to update spesefic employee

## three urls showng in console

port default : 4005
you can change it from env file

- [application base url](http://localhost:4005)
- [sawagger Admin API (private)](http://localhost:4005/admin/api-docs)
- [sawagger pulicAPI ](http://localhost:4005/api-docs)

## Admin apis names (private)

- [Admin Api create admin](http://localhost:4005/createAdmin)
- [Admin API authenticate](http://localhost:4005/authenticate)

## apis names public

- [ Api create Employee](http://localhost:4005/api/employee)[POST]
- [Api get Employees](http://localhost:4005/api/employees)[GET]
- [Api update Employees](http://localhost:4005/api/employees)[PUT]

## How to use

make sure you create a post request with login acces in the authenticate api and copy that token . after that please make sure you pasted in the x-acces-token headers for all public api requests

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`PORT`=4005
`MONGODB_URI`=mongodb://localhost:27017/pointage
`SWAGGER_PATH`=/admin/api-docs
`SWAGGER_PATH_API`=/api-docs
`TOKENVALIDITY`=24h
`SECRETKEY`=nodeRestApi

```javascript
 SWAGGER_PATH=/admin/api-docs
SWAGGER_PATH_API=/api-docs
TOKENVALIDITY=24h
SECRETKEY=nodeRestApi
```

## Running application

To run , run the following command
-to install all dependencies

```bash
  npm run install
```

to run the server

```bash
  npm run start
```
